//
//  ViewController.swift
//  WeatherApp
//
//  Created by Jovin Menezas on 27/08/20.
//  Copyright © 2020 Jovin Menezas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var searchTextFiled: UITextField!{
        didSet {
            searchTextFiled.tintColor = UIColor.lightGray
            searchTextFiled.setIcon(UIImage(named: "icon-search")!)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

}

