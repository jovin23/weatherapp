//
//  Extensions.swift
//  WeatherApp
//
//  Created by Jovin Menezas on 27/08/20.
//  Copyright © 2020 Jovin Menezas. All rights reserved.
//

import Foundation
import UIKit
extension UITextField{
    func setIcon(_ image: UIImage) {
        let iconView = UIImageView(frame:
            CGRect(x: 0, y: 5, width: 20, height: 20))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame:
            CGRect(x: 20, y: 0, width: 30, height: 30))
        iconContainerView.addSubview(iconView)
        rightView = iconContainerView
        rightViewMode = .always
    }
}
